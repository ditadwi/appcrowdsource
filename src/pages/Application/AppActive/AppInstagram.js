import React, {Component} from 'react';
import { StyleSheet, View, TouchableOpacity, Image, Dimensions } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text, Card, CardItem } from 'native-base';
import Ionicon from "react-native-vector-icons/Ionicons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import App from '../../Application/images/Application.jpg';
import { Divider } from 'react-native-elements';
import instagram from '../../Application/images/IG.png';
import {
	LineChart,
	BarChart,
	PieChart,
	ProgressChart,
	ContributionGraph,
	StackedBarChart
} from "react-native-chart-kit";


function AppInstagram(props) {
  const { navigation } = props
		return (
			<View style={styles.containerStyle}>
			<Image style={styles.bgImageStyle} source={App} />
				<Header style={styles.header}>
		          <Left>
		            <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
		              <FontAwesome5 name={'arrow-left'} size={25} color={'#69AFED'} style={{marginLeft: 10}} />
		            </TouchableOpacity>
		          </Left>
		          <Body>
		          	<Text style= {{fontSize: 21, justifyContent: 'center', color: 'white', fontWeight: 'bold'}}>Application Test</Text>
		          </Body>
		        </Header>
		        <View style={{marginHorizontal: 17}}>
		        	<Text style={styles.label}>Application Active</Text>
		        	<View style={styles.chart}>
					<LineChart
						data={{
							labels: ["10:10", "10:11", "10:12", "10:13", "10:14", "10:15", "10:16"],
							datasets: [
								{
									data: [
										Math.random() * 100,
										Math.random() * 100,
										Math.random() * 100,
										Math.random() * 100,
										Math.random() * 100,
										Math.random() * 100
									]
								}
							]
						}}
						width={Dimensions.get("window").width - 30} // from react-native
						height={250}
						yAxisLabel={"Rp"}
						chartConfig={{
							backgroundColor: "#e26a00",
							backgroundGradientFrom: "#242852",
							backgroundGradientTo: "#242852",
							decimalPlaces: 2, // optional, defaults to 2dp
							color: (opacity = 1) => `white`,
							labelColor: (opacity = 1) => `white`,
							style: {
								borderRadius: 10
							}
						}}
						bezier
						style={{
							marginVertical: 0,
							borderRadius: 10
						}}
					/>
				</View>
		        	<Text style={styles.label}>Application Active</Text>
		        	<TouchableOpacity onPress={() => navigation.navigate('ApplicationAcvite')}>
		        		<View style={styles.active}>
		        			<View style={{flexDirection: 'row', marginTop: 5, marginHorizontal: 15}}>
				        		<FontAwesome5 name={'angle-left'} size={13} color={'#ACB7CB'} style={{marginTop: 2}} />
				        		<Text style={{fontWeight: 'bold', fontSize: 12, color: '#DFEDF9', paddingLeft: 20}}>Google Play Store</Text>
				        	</View>
				        	<View style={{marginHorizontal: 15, marginTop: 10}}>
				        		<View style={{flexDirection: 'row'}}>
					        		<Image style={{marginTop: 5, width: 45, height: 45}} source={instagram} />
					        		<View>
					        			<View style={{flexDirection: 'row', paddingTop: 10, marginLeft: 6}}>
						        			<Text style={{fontSize: 12, fontWeight: 'bold', color: '#DFEDF9', paddingLeft: 5}}>Kecepatan Rata-Rata</Text>
						        		</View>
						        		<View style={{flexDirection: 'row', paddingTop: 5, marginLeft: 6}}>
						        			<Text style={{fontSize: 12, color: '#DBE7F7', paddingLeft: 5}}>Data digunakan</Text>
						        		</View>
					        		</View>
					        		<View>
					        			<View style={{flexDirection: 'row', paddingTop: 10, marginLeft: 15}}>
						        			<FontAwesome5 name={'arrow-alt-circle-down'} size={15} color={'#69AFED'}/>
						        			<View style={{flexDirection: 'row'}}>
						        				<Text style={{fontSize: 12, color: '#DBE7F7', paddingLeft: 5}}>92.0</Text>
						        				<Text style={{fontSize: 9, color: '#DBE7F7', marginTop: 2}}>Mbps</Text>
						        			</View>
						        		</View>
						        		<View style={{flexDirection: 'row', paddingTop: 5, marginLeft: 15}}>
						        			<FontAwesome5 name={'arrow-alt-circle-down'} size={15} color={'#69AFED'}/>
						        			<View style={{flexDirection: 'row'}}>
						        				<Text style={{fontSize: 12, color: '#DBE7F7', paddingLeft: 5}}>92.0</Text>
						        				<Text style={{fontSize: 9, color: '#DBE7F7', marginTop: 2}}>Mbps</Text>
						        			</View>
						        		</View>
					        		</View>
					        		<View>
					        			<View style={{flexDirection: 'row', paddingTop: 10, marginLeft: 5}}>
						        			<FontAwesome5 name={'arrow-alt-circle-up'} size={15} color={'#69AFED'}/>
						        			<View style={{flexDirection: 'row'}}>
						        				<Text style={{fontSize: 12, color: '#DBE7F7', paddingLeft: 5}}>2.90</Text>
						        				<Text style={{fontSize: 9, color: '#DBE7F7', marginTop: 2}}>Mbps</Text>
						        			</View>
						        		</View>
						        		<View style={{flexDirection: 'row', paddingTop: 5, marginLeft: 5}}>
						        			<FontAwesome5 name={'arrow-alt-circle-up'} size={15} color={'#69AFED'}/>
						        			<View style={{flexDirection: 'row'}}>
						        				<Text style={{fontSize: 12, color: '#DBE7F7', paddingLeft: 5}}>2.90</Text>
						        				<Text style={{fontSize: 9, color: '#DBE7F7', marginTop: 2}}>Mbps</Text>
						        			</View>
						        		</View>
					        		</View>
					        		
					        	</View>
				        	</View>
				        	<Divider style={{ backgroundColor: '#324573', height: 2, marginTop: 20, marginBottom: 10, marginHorizontal: 13 }} />
				        	<View style={{marginHorizontal: 15, flexDirection: 'row', justifyContent: 'space-around'}}>
				        		<View style={{width: 100, height: 70, justifyContent: 'center', alignItems: 'center'}}>
				        			<Text style={{fontSize: 12, color: '#DBE7F7'}}>Ping</Text>
				        			<Text style={{fontSize: 20, fontWeight: 'bold', color: '#DFEDF9'}}>0,0</Text>
				        			<Text style={{fontSize: 12, color: '#DBE7F7'}}>ms</Text>
				        		</View>
				        		<View style={{width: 100, height: 70, justifyContent: 'center', alignItems: 'center'}}>
				        			<Text style={{fontSize: 12, color: '#DBE7F7'}}>Jitter</Text>
				        			<Text style={{fontSize: 20, fontWeight: 'bold', color: '#DFEDF9'}}>0,0</Text>
				        			<Text style={{fontSize: 12, color: '#DBE7F7'}}>ms</Text>
				        		</View>
				        		<View style={{width: 100, height: 70, justifyContent: 'center', alignItems: 'center'}}>
				        			<Text style={{fontSize: 12, color: '#DBE7F7'}}>Packet Loss</Text>
				        			<Text style={{fontSize: 20, fontWeight: 'bold', color: '#DFEDF9'}}>0</Text>
				        			<Text style={{fontSize: 12, color: '#DBE7F7'}}>%</Text>
				        		</View>
				        	</View>
		        		</View>
		        	</TouchableOpacity>
		        </View>
			</View>
		)
	}

const styles = StyleSheet.create({
	containerStyle: {
		flex: 1
	},

	bgImageStyle: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		width: '100%',
		height: '100%'
	},
	header: {
		backgroundColor: '#2C3967'
	},
	Image: {
		width: 130,
		height: 120,
		justifyContent: 'center',
		marginLeft: 130,
		alignItems: 'center',
		marginTop: 20
	},
	label: {
		fontSize: 22,
		paddingTop: 10,
		fontWeight: 'bold',
		color: '#F9FAFB',
		marginBottom: 10
	},
	chart: {
		backgroundColor: '#242852',
		borderRadius: 10,
		marginBottom: 15,
		height: 250
	},
	active: {
		backgroundColor: '#242852',
		borderRadius: 10,
		marginBottom: 15,
		height: 190
	}
})

export default AppInstagram;