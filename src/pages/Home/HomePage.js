import * as React from 'react';
import { StyleSheet, View, TouchableOpacity, Image, SafeAreaView, ScrollView } from 'react-native';
import { Button, Text, Right } from 'native-base';
import Ionicon from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import BgImage from '../Home/images/Homee.jpg';
import Perfomance from '../Home/images/speed.png';
import App from '../Home/images/application.png';
import Log from '../Home/images/PCaP.png';
import Map from '../Home/images/map.png';
import Status from '../Home/images/status.png';
import History from '../Home/images/History.png';
import About from '../Home/images/About.png';
import setting from '../Home/images/setting.png';
import user from '../Home/images/user.png';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SigninForm from '../../pages/Signin';
import SignupForm from '../../pages/Signup';
import Setting from '../../pages/Setting/setting';
import Application from '../../pages/Application/Application';
import ApplicationSpeed from '../../pages/Application/ApplicationSpeedTest';
import AppPlaystore from '../../pages/Application/AppActive/AppPlaystore';
import AppInstagram from '../../pages/Application/AppActive/AppInstagram';
import AppFacebook from '../../pages/Application/AppActive/AppFacebook';
import AppPinterest from '../../pages/Application/AppActive/AppPinterest';
import Perfomances from '../../pages/Perfomance/index';
import settingspeed from '../../pages/Perfomance/SettingSpeed';
import status from '../../pages/Status/Status';
import PCaplog from '../../pages/PCaplog/PCaplog';

function HomeScreen({ navigation }) {
  return (
    <View style={styles.containerStyle}>
      <Image style={styles.bgImageStyle} source={BgImage} />
      <View style={styles.icon}>
        <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <FontAwesome5 name={'bars'} size={25} color={'#69AFED'} />
        </TouchableOpacity>
      </View>
      <View style={styles.titleStyle}>
        <Text style={{ fontSize: 35, width: 160, marginLeft: 25, color: 'white', fontWeight: 'bold' }}>Signal Viewer</Text>
      </View>
      <View style={styles.menustyle}>
        <TouchableOpacity style={styles.titlestyle} onPress={() => navigation.navigate('Perfomances')}>
          <Image source={Perfomance} />
          <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white', marginTop: 10 }} >Performance</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.titlestyle} onPress={() => navigation.navigate('Application')}>
          <Image source={App} />
          <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white', marginTop: 10 }} >Application</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menu}>
        <TouchableOpacity style={styles.title} onPress={() => navigation.navigate('Pcaplog')}>
          <Image source={Log} />
          <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white', marginTop: 10 }} >PCap Log</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.title}>
          <Image source={Map} />
          <TouchableOpacity>
            <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white', marginTop: 10 }} >MAP</Text>
          </TouchableOpacity>
        </TouchableOpacity>
      </View>
      <View style={styles.menu}>
        <TouchableOpacity style={styles.title} onPress={() => navigation.navigate('status')}>
          <Image source={Status} />
          <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white', marginTop: 10 }} >Status</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.title}>
          <Image source={History} />
          <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white', marginTop: 10 }} >History</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1
  },
  bgImageStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    width: '100%',
    height: '100%'
  },

  icon: {
    height: 55,
    justifyContent: 'center',
    paddingLeft: 15
  },
  titleStyle: {
    height: 90,
    justifyContent: 'center'
  },
  menustyle: {
    height: 180,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 40
  },
  titlestyle: {
    width: 130,
    height: 130,
    borderWidth: 3,
    borderColor: '#69AFED',
    borderRadius: 20,
    color: 'white',
    alignItems: 'center',
    fontSize: 14,
    fontWeight: 'bold',
    justifyContent: 'center'
  },
  menu: {
    height: 180,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingLeft: 20,
    paddingRight: 20
  },
  title: {
    width: 130,
    height: 130,
    borderWidth: 3,
    borderColor: '#69AFED',
    borderRadius: 20,
    color: 'white',
    alignItems: 'center',
    fontSize: 14,
    fontWeight: 'bold',
    justifyContent: 'center'
  },
  imgicon: {
    width: 35,
    height: 35,
    marginLeft: 45
  }
})

function MenuTab(props) {
  const { navigation } = props
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#090314', justifyContent: 'center' }}>
      <ScrollView>
        <View style={{ flex: 1, backgroundColor: '#1C253F', height: 170, justifyContent: 'center' }}>
          <View style={{ flexDirection: 'row' }}>
            <Image style={{ marginTop: 45, marginLeft: 40, color: '#69AFED', width: 70, height: 70 }} source={user} />
            <TouchableOpacity style={{ marginTop: 60, marginLeft: 15 }} onPress={() => props.navigation.navigate('Login')} >
              <Text style={{ fontSize: 14, color: 'white', fontWeight: 'bold' }}>Sign In</Text>
              <Text style={{ color: '#21B985', fontSize: 14, fontWeight: 'bold' }}>Free</Text>
            </TouchableOpacity>
          </View>
          <View style={{ width: 150, height: 30, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginLeft: 135 }}>
            <TouchableOpacity>
              <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white' }}>Why Register?</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: 1, height: 200, justifyContent: 'center' }}>
          <View style={{ justifyContent: 'center', height: 70 }}>
            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image style={styles.imgicon} source={About} />
              <Text style={{ fontSize: 25, fontWeight: 'bold', color: 'white', marginLeft: 40 }}>About</Text>
            </TouchableOpacity>
          </View>
          <View style={{ justifyContent: 'center', height: 70 }}>
            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => navigation.navigate('Setting')}>
              <Image style={styles.imgicon} source={setting} />
              <Text style={{ fontSize: 25, fontWeight: 'bold', color: 'white', marginLeft: 40 }}>Setting</Text>
            </TouchableOpacity>
          </View>
        </View>

      </ScrollView>
    </SafeAreaView>
  )
}

const Stack = createStackNavigator();

function Router() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Dashboard" component={HomeScreen} options={{ headerShown: false }} />
      <Stack.Screen name="Login" component={SigninForm} options={{ headerShown: false }} />
      <Stack.Screen name="Register" component={SignupForm} options={{ headerShown: false }} />
      <Stack.Screen name="Setting" component={Setting} options={{ headerShown: false }} />
      <Stack.Screen name="Application" component={Application} options={{ headerShown: false }} />
      <Stack.Screen name="ApplicationAcvite" component={ApplicationSpeed} options={{ headerShown: false }} />
      <Stack.Screen name="Appplaystore" component={AppPlaystore} options={{ headerShown: false }} />
      <Stack.Screen name="Appinstagram" component={AppInstagram} options={{ headerShown: false }} />
      <Stack.Screen name="Appfacebook" component={AppFacebook} options={{ headerShown: false }} />
      <Stack.Screen name="Apppinterest" component={AppPinterest} options={{ headerShown: false }} />
      <Stack.Screen name="Perfomances" component={Perfomances} options={{ headerShown: false }} />
      <Stack.Screen name="settingspeed" component={settingspeed} options={{ headerShown: false }} />
      <Stack.Screen name="status" component={status} options={{ headerShown: false }} />
      <Stack.Screen name="Pcaplog" component={PCaplog} options={{ headerShown: false }} />
    </Stack.Navigator>
  );
}

const Drawer = createDrawerNavigator();

export default function Content() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home" drawerContent={props => MenuTab(props)}>
        <Drawer.Screen name="Home" component={Router} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}