import React, {Component} from 'react';
import { StyleSheet, View, TouchableOpacity, Image } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text } from 'native-base';
import Ionicon from "react-native-vector-icons/Ionicons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import App from '../Application/images/Application.jpg';
import img from '../Application/images/AppTest.png';

function PCaplog(props) {
  const { navigation } = props
		return (
			<View style={styles.containerStyle}>
			<Image style={styles.bgImageStyle} source={App} />
				<Header style={styles.header}>
		          <Left>
		            <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
		              <FontAwesome5 name={'arrow-left'} size={25} color={'#69AFED'} style={{marginLeft: 10}} />
		            </TouchableOpacity>
		          </Left>
		          <Body>
		          	<Text style= {{fontSize: 21, justifyContent: 'center', color: 'white', fontWeight: 'bold'}}>PCap Log Test</Text>
		          </Body>
		        </Header>
		        <View>
		        	<Image style={styles.Image} source={img} />
		        </View>
		        <View style={styles.title}>
		        	<Text style= {{fontSize: 18, textAlign: 'center', color: 'white', fontWeight: 'bold'}}>Select the application to do the PCaP Log test.</Text>
		        </View>
		        <TouchableOpacity style= {styles.buttonStyle}>
					<Text style= {{fontSize: 20, textAlign: 'center', color: 'white', fontWeight: 'bold'}} >Select The Application</Text>
				</TouchableOpacity>
			</View>
		)
	}

const styles = StyleSheet.create({
	containerStyle: {
		flex: 1
	},

	bgImageStyle: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		width: '100%',
		height: '100%'
	},
	header: {
		backgroundColor: '#2C3967'
	},
	Image: {
		width: 130,
		height: 120,
		justifyContent: 'center',
		marginLeft: 130,
		alignItems: 'center',
		marginTop: 20
	},
	title: {
		width: 200,
		height: 80,
		justifyContent: 'center',
		marginLeft: 95
	},
	buttonStyle: {
		width: 330,
		height: 55,
		borderRadius: 10,
		backgroundColor: '#2C3967',
		justifyContent: 'center',
		marginHorizontal: 30 //jarak kanan kiri sama
	}
})

export default PCaplog;