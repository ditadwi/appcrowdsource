import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Image, Dimensions, ProgressBarAndroid, } from 'react-native';
import { Header, Left, Body, Text, Card, CardItem } from 'native-base';
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import App from '../Application/images/Application.jpg';
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
} from "react-native-chart-kit";
import { ProgressBar, Colors } from 'react-native-paper';


function Status(props) {
    const { navigation } = props
    return (
        <View style={styles.containerStyle}>
            <Image style={styles.bgImageStyle} source={App} />
            <Header style={styles.header}>
                <Left>
                    <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
                        <FontAwesome5 name={'arrow-left'} size={25} color={'#69AFED'} style={{ marginLeft: 10 }} />
                    </TouchableOpacity>
                </Left>
                <Body>
                    <Text style={{ fontSize: 21, justifyContent: 'center', color: 'white', fontWeight: 'bold' }}>Status</Text>
                </Body>
            </Header>
            <View style={{ marginHorizontal: 17 }}>
                <Text style={styles.label}>My Connectivity</Text>
                <View style={styles.chart}>
                    <View style={{flexDirection: 'row', marginTop: 5, marginLeft: 10}}>
                        <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18, marginRight: 10}}>2G</Text>
                        <ProgressBar progress={0.2} color={'#68AFED'} style={{width: 260, height: 10, borderRadius: 10, marginTop: 8}} />
                        <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18, marginLeft: 10}}>15%</Text>
                        
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 5, marginLeft: 10}}>
                        <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18, marginRight: 10}}>3G</Text>
                        <ProgressBar progress={0.7} color={'#EC008C'} style={{width: 260, height: 10, borderRadius: 10, marginTop: 8}} />
                        <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18, marginLeft: 10}}>75%</Text>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 5, marginLeft: 10}}>
                        <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18, marginRight: 10}}>4G</Text>
                        <ProgressBar progress={0.95} color={'#FFF200'} style={{width: 260, height: 10, borderRadius: 10, marginTop: 8}} />
                        <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18, marginLeft: 10}}>99%</Text>
                    </View>
                </View>
                <Text style={styles.label}>My Data</Text>
                <View style={styles.active}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 13 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <FontAwesome5 name={'signal'} size={25} color={'white'} />
                            <Text style={{ marginLeft: 10, fontWeight: 'bold', fontSize: 24, color: 'white', marginLeft: 20 }}>50 Mb</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <FontAwesome5 name={'wifi'} size={25} color={'white'} style={{ marginLeft: 30 }} />
                            <Text style={{ marginLeft: 10, fontWeight: 'bold', fontSize: 24, color: 'white', marginLeft: 20 }}>220 Mb</Text>
                        </View>
                    </View>
                </View>
                <Text style={styles.label}>My Data Usage</Text>
                <View style={styles.chartdata}>
                    <LineChart
                        data={{
                            labels: ["10:10", "10:11", "10:12", "10:13", "10:14", "10:15", "10:16"],
                            datasets: [
                                {
                                    data: [
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100,
                                        Math.random() * 100
                                    ]
                                }
                            ]
                        }}
                        width={Dimensions.get("window").width - 30} // from react-native
                        height={230}
                        yAxisLabel={"Rp"}
                        chartConfig={{
                            backgroundColor: "#e26a00",
                            backgroundGradientFrom: "#242852",
                            backgroundGradientTo: "#242852",
                            decimalPlaces: 2, // optional, defaults to 2dp
                            color: (opacity = 1) => `white`,
                            labelColor: (opacity = 1) => `white`,
                            style: {
                                borderRadius: 10
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 0,
                            borderRadius: 10
                        }}
                    />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1
    },

    bgImageStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: '100%',
        height: '100%'
    },
    header: {
        backgroundColor: '#2C3967'
    },
    Image: {
        width: 130,
        height: 120,
        justifyContent: 'center',
        marginLeft: 130,
        alignItems: 'center',
        marginTop: 20
    },
    label: {
        fontSize: 16,
        paddingTop: 10,
        fontWeight: 'bold',
        color: '#F9FAFB',
        marginBottom: 10
    },
    chart: {
        backgroundColor: '#242852',
        borderRadius: 10,
        marginBottom: 15,
        height: 100
    },
    chartdata: {
        backgroundColor: '#242852',
        borderRadius: 10,
        marginBottom: 15,
        height: 230
    },
    active: {
        backgroundColor: '#242852',
        borderRadius: 10,
        marginBottom: 15,
        height: 60
    }, 
    example: {
        marginVertical: 24,
        flexDirection: 'row'
      }
})

export default Status;