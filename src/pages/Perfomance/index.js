import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Image } from 'react-native';
import { Header, Left, Body, Right, Button, Text, Tab, Tabs, ScrollableTab } from 'native-base';
import Ionicon from "react-native-vector-icons/Ionicons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import App from '../Application/images/Application.jpg';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Speed from '../../pages/Perfomance/PerfomanceSpeed';
import Web from '../../pages/Perfomance/PerfomanceWeb';
import Video from '../../pages/Perfomance/PerfomanceVideo';

// const Tab = createMaterialTopTabNavigator();

function Perfomance(props) {
	const { navigation } = props
	return (
		<View style={styles.containerStyle}>
			<Image style={styles.bgImageStyle} source={App} />
			<Header style={styles.header}>
				<Left>
					<TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
						<FontAwesome5 name={'arrow-left'} size={25} color={'#69AFED'} style={{ marginLeft: 10 }} />
					</TouchableOpacity>
				</Left>
				<Body>
					<Text style={{ fontSize: 21, width: 190, justifyContent: 'center', color: 'white', fontWeight: 'bold', paddingLeft: 60 }}>Perfomance</Text>
				</Body>
				<Right>
					<TouchableOpacity onPress={() => navigation.navigate('settingspeed')}>
						<FontAwesome5 name={'cog'} size={20} color={'#69AFED'} />
					</TouchableOpacity>
				</Right>
			</Header>
			{/* <Tab.Navigator>
				<Tab.Screen name="Speed Test" component={Speed} style={styles.tab} />
				<Tab.Screen name="Web Test" component={Web} />
				<Tab.Screen name="Video Test" component={Video} />
				<Tab.Screen name="Video" component={Video} />
			</Tab.Navigator> */}

			<Tabs renderTabBar={() => <ScrollableTab />}>
				<Tab heading="Speed Test">
					<Speed />
				</Tab>
				<Tab heading="Web Test">
					<Web />
				</Tab>
				<Tab heading="Video Test">
					<Video />
				</Tab>
				<Tab heading="RF Parameters">
					<Video />
				</Tab>
			</Tabs>
		</View>
	)
}

const styles = StyleSheet.create({
	containerStyle: {
		flex: 1
	},

	bgImageStyle: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		width: '100%',
		height: '100%'
	},
	header: {
		backgroundColor: '#2C3967'
	},
	Image: {
		width: 130,
		height: 120,
		justifyContent: 'center',
		marginLeft: 130,
		alignItems: 'center',
		marginTop: 20
	},
	title: {
		width: 200,
		height: 80,
		justifyContent: 'center',
		marginLeft: 95
	},
	buttonStyle: {
		width: 330,
		height: 55,
		borderRadius: 10,
		backgroundColor: '#2C3967',
		justifyContent: 'center',
		marginHorizontal: 30 //jarak kanan kiri sama
	},
	tab: {
		backgroundColor: 'red',
		color: 'gray'
	}
})

export default Perfomance;