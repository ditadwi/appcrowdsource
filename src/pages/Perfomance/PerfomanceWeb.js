import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Image, Text } from 'react-native';
import Ionicon from "react-native-vector-icons/Ionicons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import App from '../Application/images/Application.jpg';

function Perfomancespeed(props) {
    const { navigation } = props
    return (
        <View style={styles.containerStyle}>
            <Image style={styles.bgImageStyle} source={App} />
            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                <View style={{ backgroundColor: '#1C1740', width: 280, height: 280, alignItems: 'center', justifyContent: 'center', borderRadius: 150 }}>
                    <View style={{ backgroundColor: '#242751', width: 235, height: 235, alignItems: 'center', justifyContent: 'center', borderRadius: 150 }}>
                        <View style={{ backgroundColor: '#2B3561', width: 190, height: 190, borderRadius: 150 }}>
                            <Text style={{ fontSize: 35, fontWeight: 'bold', color: 'white', justifyContent: 'center', textAlign: 'center', marginTop: 70 }}>START</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 50 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 13, color: 'white', marginLeft: 8, marginBottom: 10 }}>Website</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8, marginBottom: 10 }}>-</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8, marginBottom: 10 }}>URL</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 13, color: 'white', marginLeft: 8, marginBottom: 10 }}>Loading Time</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8, marginBottom: 10 }}>-</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8, marginBottom: 10 }}>ms</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 20 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 13, color: 'white', marginLeft: 8, marginBottom: 10 }}>Throughput</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8, marginBottom: 10 }}>-</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8, marginBottom: 10 }}>Mbps</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1
    },

    bgImageStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: '100%',
        height: '100%'
    },
    tab: {
        backgroundColor: 'red',
        color: 'gray'
    },
    text: {
        justifyContent: 'center',
        flexDirection: 'row'
    }
})

export default Perfomancespeed;