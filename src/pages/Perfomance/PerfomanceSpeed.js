import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Image, Text } from 'react-native';
import Ionicon from "react-native-vector-icons/Ionicons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import App from '../Application/images/Application.jpg';
import ping from '../Perfomance/Image/Ping.png';
import jitter from '../Perfomance/Image/Jitter.png';
import packetloss from '../Perfomance/Image/PacketLoss.png';
import download from '../Perfomance/Image/Download.png';
import upload from '../Perfomance/Image/Upload.png';

function Perfomancespeed(props) {
	const { navigation } = props
	return (
		<View style={styles.containerStyle}>
			<Image style={styles.bgImageStyle} source={App} />
			<View style={{ flexDirection: 'row', justifyContent: 'center', marginHorizontal: 5, marginTop: 25 }}>
				<View style={styles.text}>
					<Image style={{ width: 17, height: 17 }} source={ping} />
					<Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8 }}>Ping</Text>
					<Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8 }}>-</Text>
					<Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8 }}>ms</Text>
				</View>
				<View style={styles.text}>
					<Image style={{ width: 17, height: 17, marginLeft: 20 }} source={jitter} />
					<Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8 }}>Jitter</Text>
					<Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8 }}>-</Text>
					<Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8 }}>ms</Text>
				</View>
				<View style={styles.text}>
					<Image style={{ width: 17, height: 17, marginLeft: 20 }} source={packetloss} />
					<Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8 }}>Packet Loss</Text>
					<Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8 }}>-</Text>
					<Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8 }}>%</Text>
				</View>
			</View>
			<View style={{ flexDirection: 'row', marginHorizontal: 70, marginTop: 30 }}>
				<View style={{ flexDirection: 'row' }}>
					<Image style={{ width: 17, height: 17, marginLeft: 20 }} source={download} />
					<View style={{ justifyContent: 'center', alignItems: 'center' }}>
						<Text style={{ fontWeight: 'bold', fontSize: 15, color: 'white', marginLeft: 8, marginBottom: 10 }}>Download</Text>
						<Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8, marginBottom: 10 }}>-</Text>
						<Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8, marginBottom: 10 }}>Mbps</Text>
					</View>
				</View>
				<View style={{ flexDirection: 'row', marginLeft: 20 }}>
					<Image style={{ width: 17, height: 17, marginLeft: 20 }} source={upload} />
					<View style={{ justifyContent: 'center', alignItems: 'center' }}>
						<Text style={{ fontWeight: 'bold', fontSize: 15, color: 'white', marginLeft: 8, marginBottom: 10 }}>Upload</Text>
						<Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8, marginBottom: 10 }}>-</Text>
						<Text style={{ fontWeight: 'bold', fontSize: 12, color: 'white', marginLeft: 8, marginBottom: 10 }}>Mbps</Text>
					</View>
				</View>
			</View>
			<TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
				<View style={{ backgroundColor: '#1C1740', width: 280, height: 280, alignItems: 'center', justifyContent: 'center', borderRadius: 150 }}>
					<View style={{ backgroundColor: '#242751', width: 235, height: 235, alignItems: 'center', justifyContent: 'center', borderRadius: 150 }}>
						<View style={{ backgroundColor: '#2B3561', width: 190, height: 190, borderRadius: 150 }}>
							<Text style={{ fontSize: 35, fontWeight: 'bold', color: 'white', justifyContent: 'center', textAlign: 'center', marginTop: 70 }}>START</Text>
						</View>
					</View>
				</View>
			</TouchableOpacity>
			<View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 40 }}>
				<FontAwesome5 name={'signal'} size={14} color={'#66AAE7'} style={{ marginRight: 10, marginTop: 5 }} />
				<Text style={{ color: 'white', fontSize: 18, fontWeight: 'bold' }}>Telkomsel</Text>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	containerStyle: {
		flex: 1
	},

	bgImageStyle: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		width: '100%',
		height: '100%'
	},
	tab: {
		backgroundColor: 'red',
		color: 'gray'
	},
	text: {
		justifyContent: 'center',
		flexDirection: 'row'
	}
})

export default Perfomancespeed;