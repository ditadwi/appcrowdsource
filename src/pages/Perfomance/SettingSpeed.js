import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Image } from 'react-native';
import { Item, Input, Form, Label, Text, Header, Left, Body } from 'native-base';
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import App from '../Application/images/Application.jpg';

function SettingSpeed({ navigation }) {
    return (

        <View style={styles.containerStyle}>
            <Image style={styles.bgImageStyle} source={App} />
            <Header style={styles.header}>
                <Left>
                    <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
                        <FontAwesome5 name={'arrow-left'} size={25} color={'#69AFED'} style={{ marginLeft: 10 }} />
                    </TouchableOpacity>
                </Left>
                <Body>
                    <Text style={{ fontSize: 21, justifyContent: 'center', color: 'white', fontWeight: 'bold' }}>Speed Test</Text>
                </Body>
            </Header>
            <Form style={styles.formStyle}>
                <Item stackedLabel>
                    <Label style={{color: 'white', fontSize: 17, fontWeight: 'bold'}}>Test Count</Label>
                    <Input style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}></Input>
                </Item>
                <Item stackedLabel>
                    <Label style={{color: 'white', fontSize: 17, fontWeight: 'bold'}}>Server</Label>
                    <Input style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}></Input> 
                </Item>
            </Form>
        </View>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1
    },

    bgImageStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: '100%',
        height: '100%'
    },

    icon: {
        height: 55,
        justifyContent: 'center',
        paddingLeft: 15
    },

    titleStyle: {
        height: 220,
        justifyContent: 'center'
    },

    formStyle: {
        marginTop: 10,
        paddingLeft: 20,
        paddingRight: 35
    },
    itemStyle: {
        marginTop: 5
    },

    inputStyle: {
        color: 'white',
        fontSize: 17,
        fontWeight: 'bold',
        marginLeft: 5,
        paddingLeft: 20
    },
    iconStyle: {
        marginLeft: 15
    },
    header: {
        backgroundColor: '#2C3967'
    }
})

export default SettingSpeed;