import React, {Component} from 'react';
import { StyleSheet, View, TouchableOpacity, Image, Switch } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Text } from 'native-base';
import Ionicon from "react-native-vector-icons/Ionicons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import App from '../Application/images/Application.jpg';
import { Divider } from 'react-native-elements';

function Setting(props) {
  const { navigation } = props
  const [isSwicthEnabled, setSwitch] = React.useState(false)
  const [isSwicth1Enabled, setSwitch1] = React.useState(false)
  const [isSwicth2Enabled, setSwitch2] = React.useState(false)
		return (
			<View style={styles.containerStyle}>
			<Image style={styles.bgImageStyle} source={App} />
				<Header style={styles.header}>
		          <Left>
		            <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
		              <FontAwesome5 name={'arrow-left'} size={25} color={'#69AFED'} style={{marginLeft: 10}} />
		            </TouchableOpacity>
		          </Left>
		          <Body>
		            <Text style= {{fontSize: 21, justifyContent: 'center', marginLeft: 45, color: 'white', fontWeight: 'bold'}}>Settings</Text>
		          </Body>
		        </Header>
		        <View style={{marginHorizontal: 17, marginTop: 20}}>
		        	<View>
		        		<Text style={styles.text}>Contribute your data by sharing your signal data by choosing to contibute your data, it is enable us to gather data when the apps are open. All data will be anonymous to ensure user confidentiality. Our maps is crowd source, it based on real user experience. These are to inform you the actual signal whether is it good or bad. Please contribute your data to make this feature as accurate as possible.</Text>
		        		<View style={{flexDirection: 'row'}}>
		        			<Text style={styles.label}>Contribute Signal Data</Text>
		        			<Switch style={{marginTop: 20, marginLeft: 90}} 
		        				value={isSwicthEnabled}
		        				onValueChange={(value) => setSwitch(value)}
		        				trackColor={{true: '#69AFED'}}
		        			 />
		        		</View>
		        		<Divider style={{ backgroundColor: '#19123B', height: 2, marginTop: 20, marginBottom: 10 }} />
		        	</View>
		        	<View>
		        		<Text style={styles.text}>All data be anonymous to ensure user confidentiality. Please contribute your data to build the map quicker.</Text>
		        		<View style={{flexDirection: 'row'}}>
		        			<Text style={styles.label}>Track App Usage</Text>
		        			<Switch style={{marginTop: 20, marginLeft: 140}} 
		        				value={isSwicth1Enabled}
		        				onValueChange={(value) => setSwitch1(value)}
		        				trackColor={{true: '#69AFED'}}
		        			 />
		        		</View>
		        		<Divider style={{ backgroundColor: '#19123B', height: 2, marginTop: 20, marginBottom: 10 }} />
		        	</View>
		        	<View>
		        		<Text style={styles.text}>By choosing this option you are helping us to collect anonymous information your experience when using the app and allows us to improve like how often it is crashes and other condition.</Text>
		        		<View style={{flexDirection: 'row'}}>
		        			<Text style={styles.label}>Location Permission</Text>
		        			<Switch style={{marginTop: 20, marginLeft: 105}} 
		        				value={isSwicth2Enabled}
		        				onValueChange={(value) => setSwitch2(value)}
		        				trackColor={{true: '#69AFED'}}
		        			 />
		        		</View>
		        		<Divider style={{ backgroundColor: '#19123B', height: 2, marginTop: 20, marginBottom: 10 }} />
		        	</View>
		        	<View>
		        		<Text style={styles.text}>By choosing this option you can see all the speed test results on a map and find where you get the best condition.</Text>
		        		<Text style={styles.label}>Privacy Policy                                    > </Text>
		        		<Divider style={{ backgroundColor: '#19123B', height: 2, marginTop: 20, marginBottom: 10 }} />
		        	</View>
		        	<View>
		        		<Text style={{fontSize: 22, paddingTop: 5, fontWeight: 'bold', color: 'white'}}>User Policy                                         > </Text>
		        	</View>
		        </View>
			</View>
		)
	}

const styles = StyleSheet.create({
	containerStyle: {
		flex: 1
	},

	bgImageStyle: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		width: '100%',
		height: '100%'
	},
	header: {
		backgroundColor: '#2C3967'
	},
	text: {
		fontSize: 10,
		color: 'white'
	},
	label: {
		fontSize: 22,
		paddingTop: 20,
		fontWeight: 'bold',
		color: 'white'
	}
})

export default Setting;